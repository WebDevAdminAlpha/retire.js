package main

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestParseIdentifierURL(t *testing.T) {
	var tcs = []struct {
		URL  string
		Want report.Identifier
	}{
		{
			URL: "https://hackerone.com/reports/350401",
			Want: report.Identifier{
				Type:  "hackerone",
				Name:  "HACKERONE-350401",
				Value: "350401",
				URL:   "https://hackerone.com/reports/350401",
			},
		},
		{
			URL: "https://www.npmjs.com/advisories/50",
			Want: report.Identifier{
				Type:  "npm",
				Name:  "NPM-50",
				Value: "50",
				URL:   "https://www.npmjs.com/advisories/50",
			},
		},
		{
			URL: "https://nodesecurity.io/advisories/51",
			Want: report.Identifier{
				Type:  "npm",
				Name:  "NPM-51",
				Value: "51",
				URL:   "https://www.npmjs.com/advisories/51",
			},
		},
		{
			URL:  "http://osvdb.org/show/osvdb/94561",
			Want: report.OSVDBIdentifier("OSVDB-94561"),
		},
		{
			URL: "https://bugs.jquery.com/ticket/11974",
			Want: report.Identifier{
				Type:  "retire.js",
				Name:  "RETIRE-JS-48ceeb5bdae52231e03df9e98e72532e",
				Value: "48ceeb5bdae52231e03df9e98e72532e",
				URL:   "https://bugs.jquery.com/ticket/11974",
			},
		},
	}

	for _, tc := range tcs {
		name := string(tc.Want.Type)
		t.Run(name, func(t *testing.T) {
			got := parseIdentifierURL(tc.URL)
			if !reflect.DeepEqual(got, tc.Want) {
				t.Errorf("Wrong result. Expected parsing of %s to give:\n%#v\nBut got:\n%#v", tc.URL, tc.Want, got)
			}
		})
	}
}
