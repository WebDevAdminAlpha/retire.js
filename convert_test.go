package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/gitlab/security-products/tests")
}

func TestConvert(t *testing.T) {
	prependPath := "app"
	category := report.Category(report.CategoryDependencyScanning)
	scanner := report.Scanner{
		ID:   "retire.js",
		Name: "Retire.js",
	}

	var tcs = []struct {
		name                   string
		reportNodeModulesPaths string
		input                  string
		want                   *report.Report
	}{

		{
			name:                   "report node_modules paths",
			reportNodeModulesPaths: "1",
			input: `[
{
    "file": "/gitlab/security-products/tests/app/node_modules/tinycolor2/demo/jquery-1.9.1.js",
    "results": [
      {
        "version": "1.9.1",
        "component": "jquery",
        "detection": "filename",
        "vulnerabilities": [
          {
            "info": [
              "https://github.com/jquery/jquery/issues/2432",
              "http://blog.jquery.com/2016/01/08/jquery-2-2-and-1-12-released/",
              "http://research.insecurelabs.org/jquery/test/"
            ],
            "severity": "medium",
            "identifiers": {
              "issue": "2432",
              "summary": "3rd party CORS request may execute",
              "CVE": [
                "CVE-2015-2951"
              ]
            }
          },
          {
            "info": [
              "https://bugs.jquery.com/ticket/11974",
              "http://research.insecurelabs.org/jquery/test/"
            ],
            "severity": "medium",
            "identifiers": {
              "issue": "11974",
              "summary": "parseHTML() executes scripts in event handlers"
            }
          }
        ]
      }
    ]
  }
]`,
			want: &report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "3rd party CORS request may execute",
						Message:    "3rd party CORS request may execute in jquery",
						CompareKey: "app/node_modules/tinycolor2/demo/jquery-1.9.1.js:jquery:cve:CVE-2015-2951",
						Severity:   report.SeverityLevelMedium,
						Location: report.Location{
							File: "app/node_modules/tinycolor2/demo/jquery-1.9.1.js",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "jquery",
								},
								Version: "1.9.1",
							},
						},
						Identifiers: []report.Identifier{
							report.CVEIdentifier("CVE-2015-2951"),
						},
						Links: []report.Link{
							{URL: "https://github.com/jquery/jquery/issues/2432"},
							{URL: "http://blog.jquery.com/2016/01/08/jquery-2-2-and-1-12-released/"},
							{URL: "http://research.insecurelabs.org/jquery/test/"},
						},
					},
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "parseHTML() executes scripts in event handlers",
						Message:    "parseHTML() executes scripts in event handlers in jquery",
						CompareKey: "app/node_modules/tinycolor2/demo/jquery-1.9.1.js:jquery:retire.js:48ceeb5bdae52231e03df9e98e72532e",
						Severity:   report.SeverityLevelMedium,
						Location: report.Location{
							File: "app/node_modules/tinycolor2/demo/jquery-1.9.1.js",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "jquery",
								},
								Version: "1.9.1",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "retire.js",
								Name:  "RETIRE-JS-48ceeb5bdae52231e03df9e98e72532e",
								Value: "48ceeb5bdae52231e03df9e98e72532e",
								URL:   "https://bugs.jquery.com/ticket/11974",
							},
						},
						Links: []report.Link{
							{URL: "https://bugs.jquery.com/ticket/11974"},
							{URL: "http://research.insecurelabs.org/jquery/test/"},
						},
					},
				},
				Remediations:    []report.Remediation{},
				DependencyFiles: []report.DependencyFile{},
			},
		},

		{
			name:                   "convert node_modules paths",
			reportNodeModulesPaths: "0",
			input: `[
{
    "file": "/gitlab/security-products/tests/app/node_modules/tinycolor2/demo/jquery-1.9.1.js",
    "results": [
      {
        "version": "1.9.1",
        "component": "jquery",
        "detection": "filename",
        "vulnerabilities": [
          {
            "info": [
              "https://github.com/jquery/jquery/issues/2432",
              "http://blog.jquery.com/2016/01/08/jquery-2-2-and-1-12-released/",
              "http://research.insecurelabs.org/jquery/test/"
            ],
            "severity": "medium",
            "identifiers": {
              "issue": "2432",
              "summary": "3rd party CORS request may execute",
              "CVE": [
                "CVE-2015-2951"
              ]
            }
          },
          {
            "info": [
              "https://bugs.jquery.com/ticket/11974",
              "http://research.insecurelabs.org/jquery/test/"
            ],
            "severity": "medium",
            "identifiers": {
              "issue": "11974",
              "summary": "parseHTML() executes scripts in event handlers"
            }
          }
        ]
      }
    ]
  }
]`,
			want: &report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "3rd party CORS request may execute",
						Message:    "3rd party CORS request may execute in jquery",
						CompareKey: "app/package.json:jquery:cve:CVE-2015-2951",
						Severity:   report.SeverityLevelMedium,
						Location: report.Location{
							File: "app/package.json",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "jquery",
								},
								Version: "1.9.1",
							},
						},
						Identifiers: []report.Identifier{
							report.CVEIdentifier("CVE-2015-2951"),
						},
						Links: []report.Link{
							{URL: "https://github.com/jquery/jquery/issues/2432"},
							{URL: "http://blog.jquery.com/2016/01/08/jquery-2-2-and-1-12-released/"},
							{URL: "http://research.insecurelabs.org/jquery/test/"},
						},
					},
					{
						Category:   category,
						Scanner:    scanner,
						Name:       "parseHTML() executes scripts in event handlers",
						Message:    "parseHTML() executes scripts in event handlers in jquery",
						CompareKey: "app/package.json:jquery:retire.js:48ceeb5bdae52231e03df9e98e72532e",
						Severity:   report.SeverityLevelMedium,
						Location: report.Location{
							File: "app/package.json",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "jquery",
								},
								Version: "1.9.1",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "retire.js",
								Name:  "RETIRE-JS-48ceeb5bdae52231e03df9e98e72532e",
								Value: "48ceeb5bdae52231e03df9e98e72532e",
								URL:   "https://bugs.jquery.com/ticket/11974",
							},
						},
						Links: []report.Link{
							{URL: "https://bugs.jquery.com/ticket/11974"},
							{URL: "http://research.insecurelabs.org/jquery/test/"},
						},
					},
				},
				Remediations:    []report.Remediation{},
				DependencyFiles: []report.DependencyFile{},
			},
		},

		{
			name:                   "no node_modules paths",
			reportNodeModulesPaths: "",
			input: `[
  {
    "results": [
      {
        "component": "ansi2html",
        "version": "0.0.1",
        "parent": {
          "component": "sast-test-npm",
          "version": "1.0.0"
        },
        "level": 1,
        "vulnerabilities": [
          {
            "info": [
              "https://nodesecurity.io/advisories/51"
            ],
            "severity": "high"
          }
        ]
      }
    ]
  }
]`,
			want: &report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:   category,
						Scanner:    scanner,
						Message:    "Vulnerability in ansi2html",
						CompareKey: "app/package.json:ansi2html:npm:51",
						Severity:   report.SeverityLevelHigh,
						Location: report.Location{
							File: "app/package.json",
							Dependency: &report.Dependency{
								Package: report.Package{
									Name: "ansi2html",
								},
								Version: "0.0.1",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "npm",
								Name:  "NPM-51",
								Value: "51",
								URL:   "https://www.npmjs.com/advisories/51",
							},
						},
						Links: []report.Link{
							{URL: "https://nodesecurity.io/advisories/51"},
						},
					},
				},
				Remediations:    []report.Remediation{},
				DependencyFiles: []report.DependencyFile{},
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {

			if tc.reportNodeModulesPaths != "" {
				os.Setenv("REPORT_NODE_MODULES_PATHS", tc.reportNodeModulesPaths)
			} else {
				os.Unsetenv("REPORT_NODE_MODULES_PATHS")
			}

			r := strings.NewReader(tc.input)
			got, err := convert(r, prependPath)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(tc.want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tc.want, got)
			}
		})
	}
}
