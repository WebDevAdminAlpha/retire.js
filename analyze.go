package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
)

const (
	pathOutput                  = "retire.json"
	flagPythonVersion           = "python-version"
	envVarPythonVersion         = "DS_PYTHON_VERSION"
	flagJSAdvisoryDB            = "js-advisory-db"
	envVarJSAdvisoryDB          = "RETIREJS_JS_ADVISORY_DB"
	flagNodeAdvisoryDB          = "node-advisory-db"
	envVarNodeAdvisoryDB        = "RETIREJS_NODE_ADVISORY_DB"
	flagInsecure                = "advisory-db-insecure"
	envVarInsecure              = "RETIREJS_ADVISORY_DB_INSECURE"
	flagReportNodeModulePaths   = "report-node-module-paths"
	envVarReportNodeModulePaths = "REPORT_NODE_MODULE_PATHS"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagPythonVersion,
			EnvVars: []string{envVarPythonVersion},
			Usage:   "Set the version of Python to be used to install npm packages.",
		},
		&cli.StringFlag{
			Name:    flagJSAdvisoryDB,
			EnvVars: []string{envVarJSAdvisoryDB},
			Usage:   "Path or URL to the JS vulnerability data file.",
		},
		&cli.StringFlag{
			Name:    flagNodeAdvisoryDB,
			EnvVars: []string{envVarNodeAdvisoryDB},
			Usage:   "Path or URL to the Node vulnerability data file.",
		},
		&cli.BoolFlag{
			Name:    flagInsecure,
			EnvVars: []string{envVarInsecure},
			Usage:   "Enable fetching remote JS and Node vulnerability data files from hosts using an insecure or self-signed SSL (TLS) certificate.",
		},
		&cli.BoolFlag{
			Name:    flagReportNodeModulePaths,
			EnvVars: []string{envVarReportNodeModulePaths},
			Usage:   "Report on the location of the vulnerable file with node_modules prepended.",
			Value:   true,
		},
	}
}

func pathExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func setPythonVersionCommand(c *cli.Context) *exec.Cmd {
	pythonVersion := c.String(flagPythonVersion)
	if strings.HasPrefix(pythonVersion, "2") {
		log.Info("Using python 2.7")
		return exec.Command("npm", "config", "set", "python", "/usr/bin/python2.7")
	}

	log.Info("Using python 3")
	return exec.Command("npm", "config", "set", "python", "/usr/bin/python3")
}

func installDependencies(path string) error {
	log.Info("Installing dependencies")

	cmd := exec.Command("npm", "install")

	if pathExists(path + "/yarn.lock") {
		cmd = exec.Command("yarn", "install", "--ignore-engines")
	}

	cmd.Dir = path
	cmd.Env = os.Environ()

	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err
}

func retireArgs(c *cli.Context) []string {
	args := []string{"--outputformat", "jsonsimple", "--outputpath", pathOutput, "--exitwith", "0"}
	jsAdvisoryDB := c.String(flagJSAdvisoryDB)
	if jsAdvisoryDB != "" {
		log.Infof(
			"Environment variable '%s' detected, using custom value '%s'\n",
			envVarJSAdvisoryDB,
			jsAdvisoryDB)
		args = append(args, []string{"--jsrepo", jsAdvisoryDB}...)
	}

	nodeAdvisoryDB := c.String(flagNodeAdvisoryDB)
	if nodeAdvisoryDB != "" {
		log.Infof(
			"Environment variable '%s' detected, using custom value '%s'\n",
			envVarNodeAdvisoryDB,
			nodeAdvisoryDB)
		args = append(args, []string{"--noderepo", nodeAdvisoryDB}...)
	}

	if c.Bool(flagInsecure) {
		args = append(args, []string{"--insecure"}...)
	}

	return args
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// Pass additional ca cert var to node
	if c.String(cacert.FlagBundleContent) != "" {
		if err := os.Setenv("NODE_EXTRA_CA_CERTS", cacert.DefaultBundlePath); err != nil {
			return nil, err
		}
	}

	// Install node_modules needed by retire.js, if none are present
	if !pathExists(path + "/node_modules") {
		cmd := setPythonVersionCommand(c)
		stdoutStderr, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), stdoutStderr)
		if err != nil {
			log.Error("Could not set Python version")
			return nil, err
		}

		installDependencies(path)
	} else {
		log.Info("node_modules detected, skipping installation.")
		if c.Bool(flagReportNodeModulePaths) {
			os.Setenv("REPORT_NODE_MODULES_PATHS", "1")
		} else {
			os.Setenv("REPORT_NODE_MODULES_PATHS", "0")
		}
	}

	cmd := exec.Command("retire", retireArgs(c)...)
	cmd.Dir = path
	cmd.Env = os.Environ()

	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	if err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathOutput))
}
