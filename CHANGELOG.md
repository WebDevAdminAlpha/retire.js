# Retire.js analyzer changelog

## v2.11.3
- Update to Security Report Schema `v14.0.0` (!74)

## v2.11.2
- Introduce `REPORT_NODE_MODULE_PATHS` variable, to be set when `node_modules` directory shouldn't be referenced in the report (!73)

## v2.11.1
- Fix "X is a directory" error in output log (!71)

## v2.11.0
- Change permissions for Red Hat OpenShift compatibility (!70)

## v2.10.3
- Update retire.js to 2.2.4 (!65)

## v2.10.2
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!64)

## v2.10.1
- Use Node 14 (LTS version) and Alpine 3.12
- Remove build-base packages

## v2.10.0
- Update Go dependencies `common` and `urfave/cli` to remediate vulnerability GMS-2019-2 (!58)

## v2.9.4
- Update retire.js to 2.2.3 (!59)

## v2.9.3
- Use common library to parse hackerone identifier (!52)

## v2.9.2
- Update retire.js to 2.2.2

## v2.9.1
- Add `apk upgrade` command to `Dockerfile` to ensure that all installed packages are recent
- Upgrade to npm 6.14.8, musl 1.1.20-r5, musl-utils 1.1.20-r5, libcrypto1.1 1.1.1g-r0, libssl1.1 1.1.1g-r0, and ca-certificates-cacert 20191127-r2
- Remove yarn 1.15.2, which was pre-installed in the base image and not used

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!44)

## v2.8.0
- Add scan object to report (!42)

## v2.7.0

- Add support for using custom certificates when fetching npm packages (!37)

## v2.6.1

- Update retire.js to 2.2.1 (!40)

## v2.6.0

- Update logging to be standardized across analyzers (!39)

## v2.5.1

- Update to `retire v2.2.0` npm module which provides `--insecure` and `--cacert` options (!32)
- Add support for using custom certificates when fetching externally hosted vulnerability DB advisory files (!32)

## v2.5.0

- Add `id` field to vulnerabilities in JSON report (!33)

## v2.4.0

- Add support for custom CA certs (!29)

## v2.3.0

- Add `RETIREJS_JS_ADVISORY_DB` and `RETIREJS_NODE_ADVISORY_DB` vars to support air-gapped (offline) usage (!28)

## v2.2.2

- Set vulnerability location to `package.json` instead of `node_modules/*` if dependencies installed during the scan (!24)

## v2.2.1

- Fix "engine 'node' is incompatible with this module" error

## v2.2.0

- Add Python 3 to the Docker image (!14)
- Add `DS_PYTHON_VERSION` variable, to be set to `2` to switch to Python 2 (!14)

## v2.1.0

- Add support for git dependencies

## v2.0.3

- Update common to 2.1.6

## v2.0.2
- Install dependencies with npm or yarn if `node_modules` directory is missing

## v2.0.1
- Bump RetireJS to 2.0.2
- Update file identifiers for child dependencies to point to parent file location

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Bump RetireJs to 1.6.2

## v1.1.1
- Fix empty `location.file` field, use `package.json` as a default

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
