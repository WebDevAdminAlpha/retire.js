module gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2

require (
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
	golang.org/x/sys v0.0.0-20210503173754-0981d6026fa6 // indirect
)

go 1.13
